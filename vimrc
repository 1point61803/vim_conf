"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=500

" Enable filetype plugins
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" fast autoformat
nmap <leader>a :Autoformat<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Avoid garbled characters in Chinese language windows OS
let $LANG='en' 
set langmenu=en
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim

" Turn on the Wild menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Always show current position
set ruler

" show number line
set nu

" Height of the command bar
set cmdheight=2

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Don't redraw while executing macros (good performance config)
set lazyredraw 

" For regular expressions turn magic on
set magic

" indent when moving to the next line while writing code
set autoindent

" shows the last command entered in the very bottom right of Vim
set showcmd

" show a visual line under the cursor's current line
set cursorline

" show a visual line under the cursor's current column
set cursorcolumn

" Show matching brackets when text indicator is over them
set showmatch 
" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Add a bit extra margin to the left
set foldcolumn=1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable 

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

try
    colorscheme gruvbox
catch
endtry

set background=dark
colorscheme solarized8

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines


""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>


""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap VIM 0 to first non-blank character
map 0 ^

let mapleader= ","
nmap <leader>m :wincmd h<CR>
nmap <leader>/ :wincmd j<CR>
nmap <leader>; :wincmd k<CR>
nmap <leader>. :wincmd l<CR>
 
" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif


""""""""""""""""""""""""""""""
" => netrw
""""""""""""""""""""""""""""""
" removing the banner
let g:netrw_banner = 0

" open files from netrw in a previous window, unless we're opening the current dir
if argv(0) ==# '.'
    let g:netrw_browse_split = 0
else
    let g:netrw_browse_split = 4
endif

" the width of the directory explorer 
let g:netrw_winsize = 25

" the tree list style
let g:netrw_liststyle = 3

" ignore specific file
let g:netrw_list_hide = &wildignore


""""""""""""""""""""""""""""""
" => python
""""""""""""""""""""""""""""""
" enable all Python syntax highlighting features
let python_highlight_all = 1


""""""""""""""""""""""""""""""
" => gitgutter
""""""""""""""""""""""""""""""
" enable options for git gutter
set updatetime=100


""""""""""""""""""""""""""""""
" => auto-braces 
""""""""""""""""""""""""""""""
inoremap \[ \[\]<Left>

inoremap ( ()<Left>

inoremap { {}<Left>

" auto expand grouping symbols

inoremap \[<CR> \[\]<Esc>i<CR><CR><Esc>ki<Tab>

inoremap (<CR> ()<Esc>i<CR><CR><Esc>ki<Tab>

inoremap {<CR> {}<Esc>i<CR><CR><Esc>ki<Tab>

" skip over closing grouping symbols/quotes

inoremap <expr> \] getline('.')\[getpos('.')\[2\] - 1\] == '\]' ? '<Right>' : '\]'

inoremap <expr> ) getline('.')\[getpos('.')\[2\] - 1\] == ')' ? '<Right>' : ')'

inoremap <expr> } getline('.')\[getpos('.')\[2\] - 1\] == '}' ? '<Right>' : '}'

""""""""""""""""""""""""""""""
" => floaterm
""""""""""""""""""""""""""""""
" set fish shell
let g:floaterm_shell = "fish"

" width percent float term
let g:floaterm_width = 0.8

" height percent float term
let g:floaterm_height = 0.8

" toogle F10 keyboard for respawn float term
let g:floaterm_keymap_toggle = '<F10>'

""""""""""""""""""""""""""""""
" => vim move
""""""""""""""""""""""""""""""
" set modifier key normal
let g:move_key_modifier = 'A'

" width percent float term
let g:move_key_modifier_visualmode = 'A'


""""""""""""""""""""""""""""""
" => indentLine
""""""""""""""""""""""""""""""
" set identline characters
let g:indentLine_char_list = ['┆', '┊']

""""""""""""""""""""""""""""""
" => autostart command
""""""""""""""""""""""""""""""
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * :Vexplore
augroup END

set omnifunc=syntaxcomplete#Complete
